//
//  TimerUpdatableService.swift
//  LO
//
//  Created by Nikita Ilyukhin on 12.10.2020.
//  Copyright © 2019 Windmill. All rights reserved.
//

import Foundation

protocol TimerUpdatable {
    init(timeInterval: TimeInterval)
    func start(_ repeats: Bool)
    func stop()
    func resetIfNeeded(_ repeats: Bool)
}

extension TimerUpdatable {
    func start(_ repeats: Bool = false) {
        start(repeats)
    }

    func resetIfNeeded(_ repeats: Bool = false) {
        resetIfNeeded(repeats)
    }
}

final class TimerUpdatableService: TimerUpdatable {

    // MARK: - Private
    private var timeInterval: TimeInterval
    private var timer: Timer?
    private var isValid: Bool {
        if let timer = timer {
            return timer.isValid
        }

        return false
    }

    // MARK: - Public
    var fireAction: (() -> Void)?

    // MARK: - Lifecycle
    init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }

    deinit {
        invalidateTimer()
    }

    // MARK: - Actions
    func start(_ repeats: Bool = false) {
        restartTimer(repeats)
    }

    func stop() {
        invalidateTimer()
    }

    func resetIfNeeded(_ repeats: Bool = false) {
        if !isValid {
            restartTimer(repeats)
        }
    }

    // MARK: - Lifecycle
    private func restartTimer(_ repeats: Bool = false) {
        invalidateTimer()
        if #available(iOS 10.0, *) {
            timer = Timer(timeInterval: timeInterval, repeats: repeats) { [weak self] _ in
                self?.update()
            }
        } else {
            timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(update),
                          userInfo: nil,
                          repeats: repeats)
        }
        if let timer = timer {
            RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        }
    }

    private func invalidateTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }

    @objc private func update() {
        fireAction?()
    }

}
