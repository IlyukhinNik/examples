//
//  SessionExpiringService.swift
//  LO
//
//  Created by Nikita Ilyukhin on 12.10.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation
import Bank

enum SessionState {
    case active
    case expired
}

final class SessionExpiringService {

    // MARK: - Constants
    private struct Constants {
        static let sessionActivityTime = TimeInterval(5.minute)
    }

    // MARK: - Private
    private var activeSessionDateInterval: DateInterval?
    private lazy var timer: TimerUpdatable = {
        let timer = TimerUpdatableService(timeInterval: Constants.sessionActivityTime)
        timer.fireAction = sessionExpiredAction

        return timer
    }()
    private var currentDate: Date {
        return Date()
    }
    private var isExpired: Bool {
        guard let activeSessionDateInterval = activeSessionDateInterval else { return false }

        return !activeSessionDateInterval.contains(currentDate)
    }
    private var needToUpdate: Bool = false

    // MARK: - Public
    var sessionExpiredAction: (() -> Void)?
    private(set) var state: SessionState = .expired

    // MARK: - Singletone
    static let shared = SessionExpiringService()

    // MARK: - Actions
    func userActivityDetected() {
        updateSessionStateIfNeeded()
        updateIfNeeded()
    }

    // MARK: - Private actions
    private func updateSessionStateIfNeeded() {
        //If .active - always update, if .expired - update only if changed state to .active
        needToUpdate = state == .active ? true : BankSDK.shared.isUserLogged
        switch state {
        case .active:
            if isExpired || !BankSDK.shared.isUserLogged {
                state = .expired
            }
        case .expired:
            if BankSDK.shared.isUserLogged {
                state = .active
            }
        }
    }

    private func updateIfNeeded() {
        guard needToUpdate else { return }

        switch state {
        case .active:
            updateActiveSessionDateInterval()
            timer.resetIfNeeded()
        case .expired:
            timer.stop()
            sessionExpiredAction?()
        }
    }

    private func updateActiveSessionDateInterval() {
        activeSessionDateInterval = DateInterval(start: currentDate, duration: Constants.sessionActivityTime)
    }
}
