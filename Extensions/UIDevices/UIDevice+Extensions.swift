//
//  UiDevice+ModelSize.swift
//  LO
//
//  Created by Nikita Ilyukhin on 1/14/20.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

extension UIDevice {
    private var identifier: Identifier? {
            return System.name.flatMap {
                return Identifier($0)
            }
        }
    /// Device family i.e iPhone, iPod, iPad
    var deviceFamily: DeviceFamily {
        identifier.flatMap { $0.type } ?? .unknown
    }
    /// Specific model i.e iphone7 or iPhone7s
    var deviceModel: DeviceModel {
        identifier.flatMap { DeviceModel(identifier: $0) } ?? .unknown
    }
    /// Common name for device i.e "iPhone 7 Plus"
    var commonDeviceName: String {
        identifier?.description ?? "unknown"
    }
    /// Device family iPhone
    var isIphone: Bool {
        deviceFamily == .iPhone
    }
    /// Device family iPad
    var isIpad: Bool {
        deviceFamily == .iPad
    }
    /// Deivce family iPod
    var isIpod: Bool {
        deviceFamily == .iPod
    }
}
