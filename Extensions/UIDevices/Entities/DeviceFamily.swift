//
//  DeviceFamily.swift
//  g2-responsive
//
//  Created by Nikita Ilyukhin on 28.10.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation

enum DeviceFamily: String {

    case iPhone
    case iPod
    case iPad
    case unknown

    public init(rawValue: String) {
        switch rawValue {
        case "iPhone":
            self = .iPhone
        case "iPod":
            self = .iPod
        case "iPad":
            self = .iPad
        default:
            self = .unknown
        }
    }
}

// MARK: Simulator
extension DeviceFamily {
    public var isSimulator: Bool {
        #if arch(i386) || arch(x86_64)
        return true
        #else
        return false
        #endif
    }
}
