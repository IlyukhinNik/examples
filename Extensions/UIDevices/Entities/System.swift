//
//  System.swift
//  g2-responsive
//
//  Created by Nikita Ilyukhin on 28.10.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation

final class System {
    static var name: String? {
        var systemInfo = utsname()
        uname(&systemInfo)
        let encoding: UInt = String.Encoding.ascii.rawValue
        if let string = NSString(bytes: &systemInfo.machine, length: Int(_SYS_NAMELEN), encoding: encoding) {
            let identifier = (string as String).components(separatedBy: "\0").first

            // Simulator checking
            if identifier == "x86_64" || identifier == "i386" {
                return ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"]
            }
            return identifier
        }
        return nil
    }
}
