//
//  UIScreen.swift
//  g2-responsive
//
//  Created by Nikita Ilyukhin on 30.10.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

extension UIScreen {
    var width: CGFloat {
        bounds.width
    }
    var height: CGFloat {
        bounds.height
    }
    var sizeInches: Double {
        switch (height, UIScreen.main.scale) {
        case (480, _): return 3.5 //iPhone 4, 4s, etc
        case (568, _): return 4.0 //iPhone 5, 5s, 5c, iPhone SE, etc
        case (667, 3.0), (736, _): return 5.5 // iPhone 6+, etc
        case (667, 1.0), (667, 2.0): return 4.7 // iPhone 7, iPhone SE2, etc
        case (812, 3.0): return UIDevice.current.deviceModel == .iPhone12mini ? 5.4 : 5.8 // 12 Mini(5.4), X, XS, 11Pro (5.8)
        case (896, 2.0): return 6.1 // iPhone 12, iPhone 12 Pro
        case (896, 3.0): return 6.5 // iPhone XS Max, iPhone 11 Pro Max
        case (926, 3.0): return 6.7 // iPhone 12 Pro Max
        case (1024, _): return ipadSize1024()
        case (1080, _): return 10.2
        case (1112, _): return 10.5
        case (1180, _): return 10.9
        case (1194, _): return 11.0
        case (1366, _): return 12.9
        default: return 0.0
        }
    }
    var aspectRatio: String? {
        switch (height, scale) {
        case (480, _): return "3:2"
        case (568, _), (667, 3.0), (736, _), (667, 1.0), (667, 2.0): return "16:9"
        case (812, 3.0), (896, 2.0), (896, 3.0): return "19.5:9"
        case (1024, _), (1112, _), (1366, _), (1080, _): return "4:3"
        default: return nil
        }
    }

    private func ipadSize1024() -> Double {
        let deviceModel = UIDevice.current.deviceModel
        switch deviceModel {
        case .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5: return 7.9
        case .iPadPro10_5Inch: return 10.5
        default: return 9.7
        }
    }

}
