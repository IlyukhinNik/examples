//
//  BannerView.swift
//  LO
//
//  Created by Nikita Ilyukhin on 18.09.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

private struct LocalizedStringConstants {

    static var infoLogin: String { return LocalizedString("STR_LOGIN_INFO") }
    static var dismiss: String { return LocalizedString("STR_DISMISS") }
    static var infoHelp: String { return "Do you want to take a quick tour about the app?" }
    static var showTour: String { return "Show tour" }

}

final class BannerView: UIView {

    // MARK: - Constants
    private struct Constants {
        static let cornerRadius: CGFloat = 8
        static let animationTime: TimeInterval = 0.4
        static let animationDelay: TimeInterval = 0.5 //NI: for visible animation at start
    }

    // MARK: - IBOutlets
    @IBOutlet private weak var infoImageView: UIImageView!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton!

    // MARK: - Properties
    private var buttonAction: (() -> Void)?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }

    // MARK: - Setups
    private func setup() {
        guard let theme = UIApplication.shared.theme else { return }

        backgroundColor = theme.color(from: "background_box_color")
        layer.cornerRadius = Constants.cornerRadius
        infoLabel.font = theme.fonts["secondary_font"]
        infoLabel.textColor = theme.color(from: "text_color")
        actionButton.setTitleColor(theme.color(from: "badge_background_color"), for: .normal)
        actionButton.titleLabel?.font = theme.fonts["dismiss_button_font"]
        alpha = 0
    }

    // MARK: - Actions
    func show(animated: Bool = true) {
        setHidden(hidden: false, animated: animated)
    }

    func hide(animated: Bool = true) {
        setHidden(hidden: true, animated: animated)
    }

    private func setHidden(hidden: Bool, animated: Bool) {
        if animated {
            UIView.animate(withDuration: Constants.animationTime, delay: hidden ? 0 : Constants.animationDelay, animations: {
                self.alpha = hidden ? 0 : 1
            }) { _ in
                self.isHidden = hidden
            }
        } else {
            alpha = hidden ? 0 : 1
            isHidden = hidden
        }
    }

    // MARK: - Private actions
    @IBAction private func actionButtonPressed() {
        buttonAction?()
    }

}

// MARK: - Interface
extension BannerView {

    @discardableResult
    func infoImage(_ infoImage: UIImage?) -> BannerView {
        self.infoImageView.image = infoImage

        return self
    }

    @discardableResult
    func infoText(_ infoText: String) -> BannerView {
        self.infoLabel.text = infoText

        return self
    }

    @discardableResult
    func buttonTitle(_ buttonTitle: String) -> BannerView {
        self.actionButton.setTitle(buttonTitle, for: .normal)

        return self
    }

    @discardableResult
    func buttonAction(_ buttonAction: @escaping () -> Void) -> BannerView {
        self.buttonAction = buttonAction

        return self
    }

}

extension BannerView {

    static var bannerLoginView: BannerView {
        BannerView.instantiateFromNib().infoImage(UIApplication.shared.theme?.image(named: "login_info"))
                                        .infoText(LocalizedStringConstants.infoLogin)
                                        .buttonTitle(LocalizedStringConstants.dismiss)
    }

    static var bannerHelpView: BannerView {
        BannerView.instantiateFromNib().infoImage(UIApplication.shared.theme?.image(named: "help_info"))
                                        .infoText(LocalizedStringConstants.infoHelp)
                                        .buttonTitle(LocalizedStringConstants.showTour)
    }

}
