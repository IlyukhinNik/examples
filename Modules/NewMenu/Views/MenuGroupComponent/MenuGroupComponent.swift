//
//  MenuGroupComponent.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

protocol MenuGroupComponentDelegate: class {

    func menuGroupComponent(_ menuGroupComponent: MenuGroupComponent, didSelect menuItem: MenuItem)

}

class MenuGroupComponent: UIView {

    enum SeparatorStyle {

        case none
        case singleLine

    }

    private typealias Action = ((MenuItem) -> Void)?

    // MARK: - Constants
    private struct Constants {
        static let cornerRadius: CGFloat = 8.0
        static let separatorHeight: CGFloat = 1.0
    }

    // MARK: - Properties
    private var stackView = UIStackView()
    private var menuItemViews: [MenuItemView] = []
    private var headerView: MenuGroupHeaderView?

    var menuGroup: MenuGroup! {
        didSet {
            setupHeaderView()
            setupMenuItems(with: menuGroup.items)
            setupStyle()
        }
    }
    weak var delegate: MenuGroupComponentDelegate?

    // MARK: - Lifecycle
    override init(frame aRect:CGRect) {
        super.init(frame: aRect)

        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.commonInit()
    }

    private func commonInit(){
        setupStackView()
    }

    // MARK: - Setups
    private func setupStyle() {
        if menuGroup.style == .roundedBackground {
            clipsToBounds = true
            backgroundColor = UIApplication.shared.theme?.color(from: "menu_group_background_color")
            layer.cornerRadius = SlideViewConstant.cornerRadiusOfSlideView
        } else {
            backgroundColor = .clear
        }
        setupSeparatorsIfNeeded()
    }

    private func setupStackView() {
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 0
        stackView.distribution = .fill
        stackView.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(stackView)

        NSLayoutConstraint.activate([
          stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
          stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
          stackView.topAnchor.constraint(equalTo: topAnchor),
          stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
          stackView.widthAnchor.constraint(equalTo: widthAnchor)
        ])
    }

    private func setupHeaderView() {
        guard let headerInfo = menuGroup.headerInfo else { return }

        let headerView = MenuGroupHeaderView.instantiateFromNib()
                                        .item(headerInfo)
        self.headerView = headerView
        stackView.addArrangedSubview(headerView)
    }

    private func setupMenuItems(with menuItems: [MenuItem]) {
        menuItems.forEach({ item in
            let contentView = item.contentView
            contentView.onSelect = { menuItem in
                self.delegate?.menuGroupComponent(self, didSelect: menuItem)
            }
            stackView.addArrangedSubview(contentView)
        })
    }

    private func setupSeparatorsIfNeeded() {
        guard menuGroup.style == .roundedBackground else { return }

        stackView.arrangedSubviews.dropLast().forEach { view in
            let separator = UIView()
            view.addSubview(separator)
            separator.backgroundColor = UIApplication.shared.theme?.color(from: "input_separator_color")
            separator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                separator.heightAnchor.constraint(equalToConstant: Constants.separatorHeight),
                separator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                separator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                separator.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        }
        layoutIfNeeded()
    }
    
}

