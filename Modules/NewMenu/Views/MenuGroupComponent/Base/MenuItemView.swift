//
//  MenuItemView.swift
//  LO
//
//  Created by Nikita Ilyukhin on 15.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

class MenuItemView: MenuView, UIGestureRecognizerDelegate {

    // MARK: - Properties
    var item:  MenuItem!
    var onSelect: ((MenuItem) -> Void)? {
        didSet {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onViewDidTap(_:)))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            addGestureRecognizer(tapGesture)
        }
    }

    // MARK: - Actions
    @objc private func onViewDidTap(_ sender: UITapGestureRecognizer?) {
        onSelect?(item)
    }
    
}

