//
//  MenuView.swift
//  LO
//
//  Created by Nikita Ilyukhin on 15.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

class MenuView: UIView {

    // MARK: - Lifecycle
    override init(frame aRect:CGRect){
        super.init(frame: aRect)


        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.commonInit()
    }

    private func commonInit(){
        if #available(iOS 11.0, *) {
            insetsLayoutMarginsFromSafeArea = false
        }
        clipsToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
    }

}
