//
//  MenuGroupHeaderView.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

final class MenuGroupHeaderView: MenuView {

    // MARK: - IBOutlets
    @IBOutlet private(set) weak var contentView: UIView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var imageView: UIImageView!

    // MARK: - Setups
    private func setupUI() {
        guard let theme = UIApplication.shared.theme else { return }

        contentView.backgroundColor = .clear
        titleLabel.font = theme.fonts["more_menu_group_header_font"]
        titleLabel.textColor = theme.color(from: "text_color")
    }
    
}

// MARK: - Interface
extension MenuGroupHeaderView {

    @discardableResult
    func item(_ item: MenuGroup.HeaderInfo) -> MenuGroupHeaderView {
        titleLabel.text =  item.title
        imageView.image = item.image
        setupUI()

        return self
    }

}
