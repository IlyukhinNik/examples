//
//  MoreMenuItemView.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation
import UIKit

final class MoreMenuItemView: MenuItemView {

    // MARK: - IBOutlets
    @IBOutlet private(set) weak var imageView: UIImageView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var accessoryImageView: UIImageView!

    // MARK: - Variables
    override var item: MenuItem! {
        didSet {
            setup()
        }
    }
    
    // MARK: - Setups
    private func setup() {
        setupData()
        setupUI()
    }

    private func setupData() {
        titleLabel.text = item.title
        imageView.image = item.image

        switch item.accessoryType {
        case .disclosureIndicator:
            accessoryImageView.isHidden = false
            accessoryImageView.image = UIApplication.shared.theme?.image(named: "icon_arrow_right_light")
        case .checkmark:
            accessoryImageView.isHidden = true
        default :
            accessoryImageView.isHidden = true
        }
    }

    private func setupUI() {
        guard let theme = UIApplication.shared.theme else { return }

        backgroundColor = .clear
        titleLabel.font = theme.fonts["help_title_font"]
        titleLabel.textColor = theme.color(from: "text_color")
    }
    
}
