//
//  MenuListViewController.swift
//  LO
//
//  Created by Nikita Ilyukhin on 15.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation
import UIKit
import Bank

class MenuListViewController: ScrollableStackViewController, MenuGroupComponentDelegate {

    // MARK: - Constants
    private struct Constants {
        static let margin: CGFloat = 16
        static let insets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }

    // MARK: - Variables
    private var menuGroupComponents: [MenuGroupComponent] = []

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        localize()
        setupRx()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    // MARK: - Setups
    private func setupUI() {
        view.backgroundColor = UIApplication.shared.theme?.color(from: "background_color")
        viewInsets = Constants.insets
        spacing = Constants.margin
    }

    // MARK: - Actions
    func updateListItemsIfNeeded(with menuGroups: [MenuGroup]) {
        menuGroups.forEach({ menuGroup in
            if let index = menuGroupComponents.firstIndex(where: {$0.menuGroup == menuGroup}) {
                menuGroupComponents[index].menuGroup = menuGroup
            } else {
                let menuView = MenuGroupComponent()
                menuView.menuGroup = menuGroup
                menuView.delegate = self
                menuGroupComponents.append(menuView)
                addArrangedSubview(view: menuView)
            }
        })
    }

    func menuGroupComponent(_ menuGroupComponent: MenuGroupComponent, didSelect menuItem: MenuItem) {
        print("Did select: " + menuItem.title + " menu item.")
    }
}
