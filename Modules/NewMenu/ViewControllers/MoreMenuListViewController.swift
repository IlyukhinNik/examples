//
//  MoreMenuListViewController.swift
//  LO
//
//  Created by Nikita Ilyukhin on 16.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit
import RxSwift
import Bank

final class MoreMenuListViewController: MenuListViewController {

    // MARK: - Properties
    private var viewModel: MenuListViewModel = MenuListViewModel()
    var disposeBag: DisposeBag = DisposeBag()

    // MARK: - Setups
    override func localize() {
        title = LocalizedString("STR_MORE")
        tabBarItem = UITabBarItem.init(title: LocalizedString("STR_MORE"),
                                       image: UIApplication.shared.theme?.image(named: "More_inactive"),
                                       selectedImage: UIApplication.shared.theme?.image(named: "More_active"))
    }

    internal override func setupRx() {
        viewModel.menuGroups.asObservable().subscribe(onNext: { [weak self] _ in
            guard let `self` = self else { return }

            self.updateListItemsIfNeeded(with: self.viewModel.menuGroups.value)
        }).disposed(by: disposeBag)
    }

    // MARK: - Actions
    func showMarketsFlow() {
        let next = Assembly.shared.marketsController()
        push(next)
    }

    func showPaymentsFlow() {
        let next = Assembly.shared.paymentsController()

        push(next)
    }

    func showPerspectivesFlow() {
        let next = Assembly.shared.perspectivesController()
        push(next)
    }

    func showEDocumentsFlow() {
        let next = Assembly.shared.edocsController()
        push(next)
    }

    func showEventsFlow() {
        let next = Assembly.shared.eventsController()
        push(next)
    }

    func showClientCenterFlow() {
        let controller = Assembly.shared.helpController()
        navigationController?.pushViewController(controller, animated: true)
    }

    func showHelpAndSupportFlow() {
        let next = Assembly.shared.helpController()
        push(next)
    }

    func showLogoutFlow() {
        EventTracker().login.logout()
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.appDoLogout()
        transitionTo(next: Assembly.shared.initialController())
    }

    private func push(_ viewController: UIViewController, animated: Bool = true) {
        hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: animated)
        hidesBottomBarWhenPushed = false
    }

    override func menuGroupComponent(_ menuGroupComponent: MenuGroupComponent, didSelect menuItem: MenuItem) {
        switch menuItem {
        case .markets:
            showMarketsFlow()
        case .payments:
            showPaymentsFlow()
        case .perspectives:
            showPerspectivesFlow()
        case .helpAndSupport:
            showHelpAndSupportFlow()
        case .logout:
            showLogoutFlow()
        default:
            break
        }
    }
    
}
