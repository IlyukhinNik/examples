//
//  MenuItem.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

enum MenuItem: String {

    // MARK: - Menu items
    case markets
    case payments
    case advisory
    case perspectives
    case eDoc
    case events
    case lendingScreen
    case clientCenter
    case settings
    case helpAndSupport
    case logout

    // MARK: - Properties
    var title: String {
        switch self {
        case .markets:
            return LocalizedString("STR_MARKETS")
        case .payments:
            return LocalizedString("STR_PAYMENTS")
        case .advisory:
            return LocalizedString("STR_IDEAS")
        case .perspectives:
            return LocalizedString("STR_PERSPECTIVES")
        case .eDoc:
            return LocalizedString("STR_E_DOCS")
        case .events:
            return LocalizedString("STR_EVENTS")
        case .lendingScreen:
            return LocalizedString("STR_LANDING")
        case .clientCenter:
            return LocalizedString("STR_CLIENT_CENTER")
        case .settings:
            return LocalizedString("STR_SETTINGS")
        case .helpAndSupport:
            return LocalizedString("STR_HELP_AND_SUPPORT")
        case .logout:
            return LocalizedString("STR_LOGOUT")
        }
    }

    var image: UIImage? {
        switch self {
        case .markets:
            return UIApplication.shared.theme?.image(named: "menu_markets")
        case .payments:
            return UIApplication.shared.theme?.image(named: "menu_payments")
        case .advisory:
            return UIApplication.shared.theme?.image(named: "menu_advisory")
        case .perspectives:
            return UIApplication.shared.theme?.image(named: "menu_perspectives")
        case .eDoc:
            return UIApplication.shared.theme?.image(named: "menu_eDoc")
        case .events:
            return UIApplication.shared.theme?.image(named: "menu_events")
        case .lendingScreen:
            return UIApplication.shared.theme?.image(named: "menu_lendingScreen")
        case .clientCenter:
            return UIApplication.shared.theme?.image(named: "menu_clientCenter")
        case .settings:
            return UIApplication.shared.theme?.image(named: "menu_settings")
        case .helpAndSupport:
            return UIApplication.shared.theme?.image(named: "menu_helpAndSupport")
        case .logout:
            return UIApplication.shared.theme?.image(named: "menu_logout")
        default:
            return nil
        }
    }

    var module: Theme.Module? {
        switch self {
        case .markets:
            return .markets
        case .payments:
            return .payments
        case .advisory:
            return .advisory
        case .perspectives:
            return .perspective
        case .eDoc:
            return .eDocuments
        case .events:
            return .events
        case .lendingScreen:
            return .lending
        case .helpAndSupport:
            return .support
        default :
            return nil
        }
    }

    var tabBarScreen: TabBarScreen? {
        switch self {
        case .markets:
            return .markets
        case .payments:
            return .payments
        case .perspectives:
            return .perspectives
        case .eDoc:
            return .edocs
        case .events:
            return .events
        default :
            return nil
        }
    }

    var accessoryType: UITableViewCell.AccessoryType {
        switch self {
        case .markets, .payments, .advisory, .perspectives, .eDoc, .events, .lendingScreen, .clientCenter, .settings, .helpAndSupport:
            return .disclosureIndicator
        default:
            return .none
        }
    }

    var contentView: MenuItemView {
        switch self {
        case .markets, .payments, .advisory, .perspectives, .eDoc, .events, .lendingScreen, .clientCenter, .settings, .helpAndSupport, .logout:
            let moreMenuItemView = MoreMenuItemView.instantiateFromNib()
            moreMenuItemView.item = self

            return  moreMenuItemView
        }
    }

}

// MARK: - Menu objects
extension MenuItem {

    static let moreMenuItems: [MenuItem] = [.markets, .payments, .advisory, .perspectives, .eDoc, .events, .lendingScreen, .clientCenter, .settings, .helpAndSupport, .logout]

}
