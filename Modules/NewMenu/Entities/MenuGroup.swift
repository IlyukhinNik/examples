//
//  MenuGroup.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import UIKit

struct MenuGroup: Equatable {

    enum Style {

        case none
        case roundedBackground

    }

    struct HeaderInfo: Equatable {

        var title: String?
        var image: UIImage?

    }

    var headerInfo: HeaderInfo?
    var style: Style = .none
    var items: [MenuItem]
    
}

func ==(lhs: MenuGroup, rhs: MenuGroup) -> Bool {
    return lhs.items == rhs.items
}

