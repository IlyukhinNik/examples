//
//  MenuListViewModel.swift
//  LO
//
//  Created by Nikita Ilyukhin on 14.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Bank
import RxCocoa
import RxSwift

final class MenuListViewModel: ViewModel {

    // MARK: - Properties
    var disposeBag: DisposeBag = DisposeBag()
    var userSettings: UserSettingsShort = UserSettingsShort()
    var menuGroups = BehaviorRelay(value: [MenuGroup]())

    // MARK: - Lifecycle
    override init() {
        super.init()

        setupRx()
    }

    // MARK: - Setups
    private func setupRx() {
        BankSDK.shared.getUserSettings().subscribe(onNext: { [weak self] (response) in
            guard let `self` = self else { return }

            self.userSettings = response.userSettings
            self.setupData()
        }).disposed(by: disposeBag)

        Observable.of(BankSDK.shared.hasEvents, BankSDK.shared.hasPerspectives)
                  .merge()
                  .subscribe { [weak self] (_) in
                    guard let `self` = self else { return }

                    self.setupData()
                  }
                  .disposed(by: self.disposeBag)
    }

    func setupData() {
        let infoMenuGroup = MenuGroup(style: .roundedBackground, items: MenuItemsDataProvidingService.filtredAvaliableMoreMenuItems(menuGroup: .info))
        let settingsMenuGroup = MenuGroup(style: .roundedBackground, items: MenuItemsDataProvidingService.filtredAvaliableMoreMenuItems(menuGroup: .settings))
        let logoutMenuGroup = MenuGroup(style: .none, items: MenuItemsDataProvidingService.filtredAvaliableMoreMenuItems(menuGroup: .logout))

        menuGroups.accept([infoMenuGroup, settingsMenuGroup, logoutMenuGroup])
    }

}
