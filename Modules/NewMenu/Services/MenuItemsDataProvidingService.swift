//
//  MenuItemsDataService.swift
//  LO
//
//  Created by Nikita Ilyukhin on 15.11.2020.
//  Copyright © 2020 Windmill. All rights reserved.
//

import Foundation
import UIKit
import Bank

enum TypeMenuGroup: String {

    case info
    case settings
    case logout

}

final class MenuItemsDataProvidingService {

    // MARK: - Constant properties
    static let configurationDependsModules: [Theme.Module] = [.support, .onboardingTour]

    // MARK: - Computed properties
    static var avaliableBankSDKModules: [Theme.Module] {
        var avaliableModules: [Theme.Module] = []

        if BankSDK.shared.hasMarkets.value { avaliableModules.append(.markets) }
        if BankSDK.shared.hasPayments.value { avaliableModules.append(.payments) }
        if BankSDK.shared.hasIdeas.value { avaliableModules.append(.advisory) }
        if BankSDK.shared.hasPerspectives.value { avaliableModules.append(.perspective) }
        if BankSDK.shared.hasEDocuments.value { avaliableModules.append(.eDocuments) }
        if BankSDK.shared.hasEvents.value { avaliableModules.append(.events) }
        if BankSDK.shared.hasMessages.value { avaliableModules.append(.messaging) }
        if BankSDK.shared.hasCredit.value { avaliableModules.append(.lending) }

        return avaliableModules
    }
    static var avaliableConfigurationModules: [Theme.Module] {
        UIApplication.shared.theme?.configuration.filter{ avaliableBankSDKModules.contains($0) && !configurationDependsModules.contains($0) } ?? []
    }
    static var avaliableAppModules: [Theme.Module] {
        UIApplication.shared.theme?.configuration.filter{ avaliableConfigurationModules.contains($0) || configurationDependsModules.contains($0) } ?? []
    }

}

// MARK: - Data
extension MenuItemsDataProvidingService {

    // MARK: - Constant properties
    static let requiredMoreMenuItems: [MenuItem] = [.clientCenter, .settings, .logout]
    static let requiredMoreAbsentTabs: [TabBarScreen] = [.messaging, .edocs, .markets, .events]//show menu item if no Tab for Module

    static let infoMoreMenuItems: [MenuItem] = [.markets, .payments, .advisory, .perspectives, .eDoc, .events, .lendingScreen]
    static let settingsMoreMenuItems: [MenuItem] = [.clientCenter, .settings, .helpAndSupport]
    static let logoutMoreMenuItems: [MenuItem] = [.logout]

    // MARK: - Static actions
    static func filtredAvaliableMoreMenuItems(menuGroup: TypeMenuGroup) -> [MenuItem] {
        switch menuGroup {
        case .info:
            return filtredAvaliableMoreMenuItems(items: infoMoreMenuItems)
        case .settings:
            return filtredAvaliableMoreMenuItems(items: settingsMoreMenuItems)
        case .logout:
            return filtredAvaliableMoreMenuItems(items: logoutMoreMenuItems)
        }
    }

    static func filtredAvaliableMoreMenuItems(items: [MenuItem]) -> [MenuItem] {
        items.filter { item -> Bool in
            if MenuItemsDataProvidingService.requiredMoreMenuItems.contains(where: { $0.module == item.module }) {
                return true
            } else if let module = item.module, MenuItemsDataProvidingService.avaliableAppModules.contains(module){
                if let tabBarScreen = item.tabBarScreen {
                    return !Assembly.shared.tabBarController().tabs.contains(tabBarScreen)
                } else {
                    return true
                }
            }
            return false
        }
    }

}

